'use strict';
module.exports = function(grunt) {

    // load all grunt tasks matching the `grunt-*` pattern
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        // watch for changes and trigger compass, jshint, uglify and livereload
        watch: {
            compass: {
                files: ['_assets/styles/**/*.{scss,sass}'],
                tasks: ['compass']
            },
            js: {
                files: '<%= jshint.all %>',
                tasks: ['jshint', 'uglify']
            },
            images: {
                files: ['_assets/images/**/*.{png,jpg,gif}'],
                tasks: ['imagemin']
            },
            source: {
                files: ['_assets/slim/**/*.{slim}'],
                tasks: ['slim']
            },
            sprite: {
                files: ['_assets/images/_sprite/*.{png,jpg,gif}'],
                tasks: ['sprite']
            }
        },

        // compass and scss
        compass: {
            dist: {
                options: {
                    config: 'config.rb',
                    force: true
                }
            }
        },
        //sprite generation
        sprite:{
            all: {
                src: '_assets/images/_sprite/*.{png,gif,jpg}',
                destImg: '_public/sprite.png',
                destCSS: '_assets/styles/vendor/sprites.scss'
            }
        },

        // javascript linting with jshint
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                "force": true
            },
            all: [
                'Gruntfile.js',
                '_assets/js/**/*.js'
            ]
        },
        
        //Slim
        slim: {
            dist: {
                files: {
                    'index.html': '_assets/slim/index.slim'
                }
            }
        },

        // uglify to concat, minify, and make source maps
        uglify: {
            main: {
                files: {
                    '_public/main.v91.js': [
                        '_assets/js/main.v91.js'
                    ]
                }
            }
        },

        // image optimization
        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 7,
                    progressive: true,
                    interlaced: true
                },
                files: [{
                    expand: true,
                    cwd: '_assets/images/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: '_public/'
                }]
            }
        },

    });
    

    // rename tasks
    grunt.renameTask('rsync', 'deploy');

    // register task
    grunt.registerTask('default', ['compass', 'uglify', 'imagemin', 'watch', 'jshint', 'slim']);

    // load tasks
    grunt.loadNpmTasks('grunt-spritesmith');


};