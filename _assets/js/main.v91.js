var tag = document.createElement('script');
tag.src = "//www.youtube.com/iframe_api";

var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;

function onYouTubeIframeAPIReady() {
	player = new YT.Player('player', {
	  	height: $(window).height(),
	  	width: $(window).height(),
	  	videoId: '0FcDXL5Aw0o',
	  	playerVars: {
	  		controls: 0,
	  		showinfo: 0,
	  		wmode: 'transparent'
	  	},
	  	events: {
		    'onReady': onPlayerReady
	  	}
	});
}

function onPlayerReady(event) {
	event.target.playVideo();
}